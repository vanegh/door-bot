# Telegram Door Bot #

Telegram Bot that tells you if the main entrance Door is open or close. 

---
## Hardware

### ESP-01S (ESP8266)
#### General Information
Information in Spanish about configuration, pinout, datasheet can be found in the following link:

* [General information](https://cdmxelectronica.com/producto/modulo-wifi-esp8266-esp-01s/)

### MC-38 Magnetic Sensor Switch

### Wiring

## Code

## Telegram Bot

To access the bot, find the **@HausofmeisterBot** username on Telegram.

### Commands

## /start

![start_doorbot](images/start_doorbot.png)  

## /help

![help_doorbot](images/help_doorbot.png)

## /status
![status_doorbot](images/status_doorbot.png)




### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact