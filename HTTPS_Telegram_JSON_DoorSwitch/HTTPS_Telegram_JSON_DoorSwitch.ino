/*
 *  
 */
#include "esp_data.h"
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <WiFiClientSecure.h>

// Wifi Constants
const char* ssid = HOME_SSID;
const char* password = HOME_PASS;

// Telegram API constants
const char* host = TELEGRAM_HOST;
const int httpsPort = TELEGRAM_PORT;
const char bot_token[]= BOT_TOKEN;
// SHA1 fingerprint of the certificate
const char* fingerprint = FINGERPRINT;

//CouchDB Constants
const char couchdbUser[] = COUCHDB_USER;
const char couchdbPassword[] = COUCHDB_PWD;
const char couchdbHost[]=COUCHDB_HOST;
const int  couchdbPort=COUCHDB_PORT;

String line;
String msg;
int doorSwitchState=0;
int present=0;
long resultUpdateID=0;
long resultMessageChatID=0;
int flag=0;

// Input / Output Variables for ESP8266 ESP-01
const int switchPin=0;
const int ledPin=2;


WiFiClientSecure client;
                           

void setup() {
  pinMode(switchPin,INPUT_PULLUP);
  pinMode(ledPin,OUTPUT);
  digitalWrite(switchPin, HIGH);
  
  
  Serial.begin(115200);
  wifiConnect();
  //tlsConnection();
  //testHttps();
 
    
}

void loop() {
  getUpdatesTelegram();
  if (flag){
    sendUsersUpdatesDoor();                             
    }    
}
  


//  Functions
void wifiConnect(){
  Serial.println();
  Serial.print("connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP()); 
  }
  
void tlsConnection(){
  Serial.print("connecting to ");
  Serial.println(host);
  client.setFingerprint(fingerprint);

  if (client.verify(fingerprint, host)) {
    Serial.println("certificate matches");
  } 
  else {
    Serial.println("certificate doesn't match");
  } 
 
  if (!client.connect(host, httpsPort)) {
    Serial.println("connection failed");
    return;
  }
}

void testHttps(){
  String url = "/bot"+String(bot_token)+"/getMe";
  Serial.print("Requesting URL: ");
  client.println(String("GET ") + url + " HTTP/1.1");
  client.println("Host: " + String(host));
  client.println("Connection: close\r\n\r\n");
  printPayload();   
  
  }
String printPayload(){
  Serial.println("request sent");
  while (client.connected()) {
    String line = client.readStringUntil('\n');
    if (line == "\r") {
      Serial.println("headers received");
      break;
    }
  }
  String line = client.readStringUntil('\r'); 

  
  Serial.println(line); 
  return line; 
}


void sendMessageTelegram(String msg, long chatID){
  tlsConnection();
  //long chatID=419609479;
  String url = "/bot"+String(bot_token)+"/sendMessage?chat_id="+chatID+"&text="+msg;
  Serial.print("requesting URL: ");
  client.println(String("POST ") + url + " HTTP/1.1");
  client.println("Host: " + String(host));
  client.println("Connection: close\r\n\r\n");
  printPayload();   
  }
  
void getUpdatesTelegram(){
  String line;
  tlsConnection();
  String url = "/bot"+String(bot_token)+"/getUpdates?limit=1&offset="+String(resultUpdateID);
  Serial.print("requesting URL: ");
  client.println(String("GET ") + url + " HTTP/1.1");
  client.println("Host: " + String(host));
  client.println("Connection: close\r\n\r\n");
  line=printPayload();

  const size_t capacity = 2*JSON_ARRAY_SIZE(1) + 2*JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(3) + JSON_OBJECT_SIZE(4) + JSON_OBJECT_SIZE(5) + JSON_OBJECT_SIZE(6) + 240;
  DynamicJsonDocument doc(capacity);
  // Parse JSON object
  DeserializationError error = deserializeJson(doc, line);
  if (error) {      
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.c_str());  
  }
  JsonObject result =doc["result"][0];
  resultUpdateID = result["update_id"]; 
  
  if (!result.isNull()){
     const long resultMessageFromID=result["message"]["from"]["id"];
     resultMessageChatID=result["message"]["chat"]["id"];
     const char* resultMessageText = result["message"]["text"];
     if (verifyUser(resultMessageFromID)){
      sendMessageTelegram(msg,resultMessageChatID);
     }
     else {
      if (strcmp(resultMessageText, "/start")== 0){
        msg="Hello, I am a chatbot that tells you if your door is open or closed. I will inform you of a change of state automatically. Type /help for more information. Otherwise...";   
        flag=1;
        sendMessageTelegram(msg,resultMessageChatID);
      }
     else if (strcmp(resultMessageText, "/help")== 0){
        msg="type /status to know if the door is open or closed. Otherwise, I will inform you of a change of state automatically. I am a very easy Bot :)";
        sendMessageTelegram(msg,resultMessageChatID);
      }
     else if (strcmp(resultMessageText,"/status")==0){
        sendStatusDoor(resultMessageChatID);
      }   
     else {
        msg="Invalid command. Type /help for more information.";
        sendMessageTelegram(msg,resultMessageChatID);    
      }
      
      Serial.println(resultMessageText);
      Serial.println(resultMessageChatID);
      Serial.println(resultMessageFromID);
      resultUpdateID+=1;
      Serial.println(resultUpdateID);
           
      }    
      
  }
}

void sendUsersUpdatesDoor(){
  doorSwitchState=digitalRead(switchPin);
  if (present != doorSwitchState){
    present=doorSwitchState;
    Serial.println(doorSwitchState);
    if(doorSwitchState == LOW){ 
      msg="The door is closed";      
    }
  else if (doorSwitchState==HIGH){
    digitalWrite(ledPin,HIGH);
    msg="The door is open";
  }
  WiFiClient client;  
  HTTPClient http;
  const size_t capacity = 3*JSON_ARRAY_SIZE(2) + 3*JSON_OBJECT_SIZE(3)+380;
  DynamicJsonDocument doc(capacity);
  Serial.print("[HTTP] begin...\n");
  if (http.begin(client, "http://"+String(couchdbUser)+":"+String(couchdbPassword)+"@"+String(couchdbHost)+":"+couchdbPort+"/telegram/_design/doorBot/_view/user")) {  // HTTP
      Serial.print("[HTTP] GET...\n");
      // start connection and send HTTP header
      int httpCode = http.GET();
      if (httpCode > 0) {
        // HTTP header has been send and Server response header has been handled
        Serial.printf("[HTTP] GET... code: %d\n", httpCode);
        if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
          String payload = http.getString();
          //Serial.println(payload);
          DeserializationError error = deserializeJson(doc, payload);
          if (error) {      
            Serial.print(F("deserializeJson() failed: "));
            Serial.println(error.c_str());  
          }
          //const int totalRows =doc["total_rows"];
          JsonArray users=doc["rows"];
          //int count=users.size();
          //Serial.println(count);
          for (JsonObject row: users){
            const long chatID =row["key"];
            Serial.println(chatID);
            //sendupdates to the Chatbot users :)
            sendMessageTelegram(msg,chatID);
            }
        }

      
      }
      else{
        Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());       
      }

  }
  else{
      Serial.printf("[HTTP} Unable to connect\n");
  }    
    
  } 
}


void sendStatusDoor(long chatID){
  doorSwitchState=digitalRead(switchPin);
  Serial.println(doorSwitchState);
  if(doorSwitchState == LOW){ 
    msg="The door is closed";      
    }
  else if (doorSwitchState==HIGH){
    digitalWrite(ledPin,HIGH);
    msg="The door is open";
  }
  sendMessageTelegram(msg,chatID);
} 


 bool verifyUser(long chatID){
  WiFiClient client;  
  HTTPClient http;
  String msg="";
  const size_t capacity = JSON_ARRAY_SIZE(1) + JSON_ARRAY_SIZE(2) + 2*JSON_OBJECT_SIZE(3)+223;
  DynamicJsonDocument doc(capacity);
  Serial.print("[HTTP] begin...\n");
  
  if (http.begin(client, "http://"+String(couchdbUser)+":"+String(couchdbPassword)+"@"+String(couchdbHost)+":"+couchdbPort+"/telegram/_design/doorBot/_view/user?key="+chatID)) {  // HTTP
      Serial.print("[HTTP] GET...\n");
      // start connection and send HTTP header
      int httpCode = http.GET();

      // httpCode will be negative on error
      if (httpCode > 0) {
        // HTTP header has been send and Server response header has been handled
        Serial.printf("[HTTP] GET... code: %d\n", httpCode);

        // file found at server
        if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
          String payload = http.getString();
          DeserializationError error = deserializeJson(doc, payload);
          if (error) {      
            Serial.print(F("deserializeJson() failed: "));
            Serial.println(error.c_str());  
          }
          //Serial.println(payload);
          JsonObject row =doc["rows"][0];
          
          /*const long userID=row["key"];
          const char* firstName=row["value"][0];
          const char* lastName=row["value"][1];
          Serial.println(row);
          Serial.println(userID);
          Serial.println(firstName);
          Serial.println(lastName); */ 
                 
          return row.isNull();
           
          
        }
      } else {
        Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
      }

      http.end();
    } else {
      Serial.printf("[HTTP} Unable to connect\n");
    }
  }











  
